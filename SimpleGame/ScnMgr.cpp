#include "stdafx.h"
#include "ScnMgr.h"


ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(500, 500);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//obj initialize
	for (int i = 0; i < MAX_OBJ_NUM; i++)
	{
		m_obj[i] = NULL;
	}

	////test obj class
	//m_TestOBJ = new Object();
	//m_TestOBJ->SetLocation(0,0,0);
	//m_TestOBJ->SetSize(100,100,100);
	//m_TestOBJ->SetColor(1,1,1,1);

	for (int i = 0 ; i<10; i++) 
	{
		m_TestChar[i] = AddObject
	}
}

ScnMgr::~ScnMgr()
{
	delete m_Renderer;
	m_Renderer = NULL;
}

void ScnMgr::RenderScene()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Drew all m_objs

	for (int i = 0; i < MAX_OBJ_NUM; i++) 
	{
		if(m_obj[i]!=NULL)
		{
		float x, y, z = 0;
		float sx, sy, sz = 0;
		float r, g, b, a = 0;
		m_obj[i]->GetLocation(&x, &y, &z);
		m_obj[i]->GetSize(&sx, &sy, &sz);
		m_obj[i]->GetColor(&r, &g, &b, &a);
		m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);
		}
	}

	


}

int ScnMgr::AddObject(float x, float y, float z, float sx, float sy, float sz, float r, float g, float b, float a)
{
	int idx = -1;

	for (int i = 0; i < MAX_OBJ_NUM ;i++)
	{
		if (m_obj[i] == NULL)
		{
			idx = i;
			break;
		}
	}
	if (idx == -1)
	{
		std::cout << "no more empty obj slot\n";
		return -1;
	}
	m_obj[idx] = new Object();
	m_obj[idx]->SetLocation(x, y, z);
	m_obj[idx]->SetSize(sx, sy, sz);
	m_obj[idx]->SetColor(r, g, b, a);
}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0) 
	{
		std::cout << "input idx is negative :" << idx << "\n";
	}
	if (idx > MAX_OBJ_NUM)
	{
		std::cout << "input idx is negative :" << idx << "\n";
	}
	if (m_obj[idx] ==NULL) 
	{
		std::cout << "m_obj is null" << idx << "\n";
	}
	delete m_obj[idx];
	m_obj[idx] = NULL;
}



