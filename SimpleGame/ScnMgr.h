#pragma once
#include "Globals.h"
#include "Renderer.h"
#include "Object.h"


class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();
	void RenderScene();
	int AddObject(float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a);
	void DeleteObject(int idx);


private:
	Renderer* m_Renderer = NULL;
	Object* m_TestOBJ = NULL;
	Object* m_obj[MAX_OBJ_NUM];

};

