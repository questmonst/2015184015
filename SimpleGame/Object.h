#pragma once
class Object
{
public:
	Object();
	~Object();
	void SetSize(float x,float y,float z);
	void GetSize(float * sx,float * sy, float * sz);
	void SetLocation(float x, float y, float z);
	void GetLocation(float * x, float * y, float * z);
	void SetColor(float r, float g, float b, float a);
	void GetColor(float *r, float *g, float *b, float *a);
private:
	float m_x, m_y, m_z;//location
	float m_sx, m_sy, m_sz;//size
	float m_r, m_g, m_b, m_a;//color

};

